#!/bin/sh

while sleep 1
do
    echo "Listening for serial device"
    socat -v tcp-l:4000,fork,keepalive,nodelay,forever,reuseaddr $SERIAL,raw,echo=0
    echo "Serial device disconnected. Restarting."
done
