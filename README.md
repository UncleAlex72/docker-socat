# docker-socat
A docker image for communicating with a serial device via TCP. Use the SERIAL
environment variable to set which serial device to communicate with.
